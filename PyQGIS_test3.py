import time
start = time.time()
from osgeo import gdal
import glob, time, subprocess, qgis, sys, os, shutil
import numpy as np
import geopandas as gpd

sys.path.append(r"C:\Program Files\QGIS 3.28.8\apps\qgis-ltr\python\plugins")
from PyQt5.QtGui import *
from qgis.core import *
app = QgsApplication([],True, None)
QgsApplication.initQgis()
import processing
# From processing.algs.gdal.merge import *
# alg = merge()

from processing.algs.gdal.merge import merge
alg = merge()

# from processing.algs.ClipRasterByMask import *
# clip = ClipRasterByMask ()

from qgis.core import QgsApplication, QgsProcessingFeedback
from qgis.analysis import QgsNativeAlgorithms

# Supply the path to the QGIS installation folder
QgsApplication.setPrefixPath("C:\Program Files\QGIS 3.28.8\apps\qgis-ltr\python\qgis", True)

# Create and initialize the QGIS application
qgs = QgsApplication([], False)
qgs.initQgis()

# Import the processing module and register the native algorithms
import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

# Set up the processing feedback object
feedback = QgsProcessingFeedback()
from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterFile
from qgis.core import QgsProcessingParameterVectorLayer
from qgis.core import QgsProcessingParameterString
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsProcessingParameterFeatureSink
from qgis.core import QgsProcessingParameterDefinition
from qgis.core import QgsExpression

grid_size = 1000
sexes =['f' ,'m']
ages = [0, 1, 5, 10, 15, 20, 25, 30, 35]
target_countries = ['BEN'] #, 'MDG']
equivalence_crs = {'MDG': 
                    {'OPERATION': '+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=push +v_3 +step +proj=cart +ellps=WGS84 +step +proj=helmert +x=198.383 +y=240.517 +z=107.909 +step +inv +proj=cart +ellps=intl +step +proj=pop +v_3 +step +proj=omerc +lat_0=-18.9 +lonc=44.1 +alpha=18.9 +gamma=18.9 +k=0.9995 +x_0=400000 +y_0=800000 +ellps=intl +pm=paris',
                    'TARGET_CRS': 29702    
                    },
                    'GNQ':
                    {'OPERATION': '+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=32 +ellps=WGS84',
                    'TARGET_CRS': 32632
                    },
                    'BEN':
                    { 'OPERATION': '+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=31 +ellps=WGS84',
                    'TARGET_CRS': 32631
                    }
                }

for country in target_countries:

    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem.fromEpsgId(equivalence_crs[country]['TARGET_CRS']))

    # CHANGE
    os.chdir(os.path.join(os.environ['USERPROFILE'], "BOX", "IIEP_DataSets", "School_age_Populations"))

    # Obtaining the bounding coordinates to create the corresponding grid, and then clip it
    # We start by dissolving the UN SALB boundaries
    administrative_boundaries = os.path.join('/vsizip', os.environ['USERPROFILE'], "BOX", "IIEP_DataSets", "UN_SALB_Boundaries_Shapefiles", 'BNDA_' + country + ".zip")
    alg_params = {'INPUT': administrative_boundaries,
                    'FIELD':[],
                    'SEPARATE_DISJOINT':False,
                    'OUTPUT':'TEMPORARY_OUTPUT'}
    dissolved_boundaries = processing.run("native:dissolve", alg_params)

    # We reproject the administrative boundaries to a projected reference system 
    alg_params = {'INPUT': dissolved_boundaries['OUTPUT'],
    'TARGET_CRS': QgsCoordinateReferenceSystem("EPSG:" + str(equivalence_crs[country]['TARGET_CRS'])),
    'OPERATION': equivalence_crs[country]['OPERATION'],
    'OUTPUT': 'TEMPORARY_OUTPUT'
    }
    reprojected_boundaries = processing.run("native:reprojectlayer", alg_params)

    # We then calculate the bounding box
    bounding_box = os.path.join(os.getcwd(), country, "bounding_box.gpkg")
    alg_params = {'INPUT': reprojected_boundaries['OUTPUT'],
                    'OUTPUT': bounding_box}
    processing.run("native:boundingboxes", alg_params)

    # And extract the coordinates
    box = gpd.read_file(bounding_box)
    g = [i for i in box.geometry]
    x,y = g[0].exterior.coords.xy
    coords = np.dstack((x,y)).tolist()

    # Now that we have this we can finally create the grid
    grid_extent = str(coords[0][0][0]) + "," + str(coords[0][1][0]) + "," + str(coords[0][0][1]) + "," + str(coords[0][2][1]) + " [EPSG:" + str(equivalence_crs[country]['TARGET_CRS']) + "]"
    alg_params = {'TYPE':2,
                    'EXTENT':grid_extent,
                    'HSPACING':grid_size,
                    'VSPACING':grid_size,
                    'HOVERLAY':0,
                    'VOVERLAY':0,
                    'CRS':QgsCoordinateReferenceSystem('EPSG:' + str(equivalence_crs[country]['TARGET_CRS'])),
                    'OUTPUT': 'TEMPORARY_OUTPUT'}
    temporary_grid = processing.run("native:creategrid", alg_params)

    # Finally we clip the grid using the UN SALB boundaries
    overlay = os.path.join('/vsizip', os.environ['USERPROFILE'], "BOX", "IIEP_DataSets", "UN_SALB_Boundaries_Shapefiles", 'BNDA_' + country + ".zip")
    alg_params = {'INPUT': temporary_grid['OUTPUT'],
                    'OVERLAY':overlay,
                    'OUTPUT': 'TEMPORARY_OUTPUT'}
    clipped_grid = processing.run("native:clip", alg_params)

    for year in range(2000,2002,1):
        print("Working on year " + str(year))
        # Calculating the raster statistics for every age group
        for sex in sexes:
            for age in ages:
                if sex == 'f' and age == 0:
                    administrative_boundaries = clipped_grid['OUTPUT']
                else:
                    administrative_boundaries = results_zonal_statistics['OUTPUT']
                
                raster_file = os.path.join(os.getcwd(), country, country.lower() + "_" + sex + "_" + str(age) + "_" + str(year) + ".tif")
                alg_params = {
                    'COLUMN_PREFIX': sex.upper() + "_" + str(age) + "_",
                    'INPUT': administrative_boundaries,
                    'INPUT_RASTER': raster_file,
                    'RASTER_BAND': 1,
                    'STATISTICS': [1],  # Sum
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }

                results_zonal_statistics = processing.run('native:zonalstatisticsfb', alg_params)
                print("Run " + sex.upper() + "_" + str(age))

        # Creating the 0 to 4 age groups
        alg_params = {
            'FIELDS_MAPPING': [{'expression': '@row_number','length': 0,'name': 'row_number','precision': 0,'type': 2}, {'expression': '"F_5_sum"','length': 0,'name': 'F_5_sum','precision': 0,'type': 6},{'expression': '"F_10_sum"','length': 0,'name': 'F_10_sum','precision': 0,'type': 6},{'expression': '"F_15_sum"','length': 0,'name': 'F_15_sum','precision': 0,'type': 6},{'expression': '"F_20_sum"','length': 0,'name': 'F_20_sum','precision': 0,'type': 6},{'expression': '"F_25_sum"','length': 0,'name': 'F_25_sum','precision': 0,'type': 6},{'expression': '"F_30_sum"','length': 0,'name': 'F_30_sum','precision': 0,'type': 6},{'expression': '"F_35_sum"','length': 0,'name': 'F_35_sum','precision': 0,'type': 6},{'expression': 'M_5_sum','length': 0,'name': 'M_5_sum','precision': 0,'type': 6},{'expression': 'M_10_sum','length': 0,'name': 'M_10_sum','precision': 0,'type': 6},{'expression': 'M_15_sum','length': 0,'name': 'M_15_sum','precision': 0,'type': 6},{'expression': 'M_20_sum','length': 0,'name': 'M_20_sum','precision': 0,'type': 6},{'expression': 'M_25_sum','length': 0,'name': 'M_25_sum','precision': 0,'type': 6},{'expression': 'M_30_sum','length': 0,'name': 'M_30_sum','precision': 0,'type': 6},{'expression': 'M_35_sum','length': 0,'name': 'M_35_sum','precision': 0,'type': 6},{'expression': '"F_0_sum" + "F_1_sum"','length': 0,'name': 'F_0_sum','precision': 0,'type': 6},{'expression': '"M_0_sum" + "M_1_sum"','length': 0,'name': 'M_0_sum','precision': 0,'type': 6}],
            'INPUT': results_zonal_statistics['OUTPUT'],
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
        groups_0_4_output = processing.run('native:refactorfields', alg_params)

        # Calculating single years of age - Section 1
        alg_params = {
            'FIELDS_MAPPING': [{'expression': '"row_number"','length': 0,'name': 'row_number','precision': 0,'type': 2}, {'expression': '$area','length': 0,'name': 'Area','precision': 0,'type': 6},{'expression': ' 0.3616*"M_0_sum" - 0.2768*"M_5_sum" + 0.1488*"M_10_sum" - 0.0336*"M_15_sum"  ','length': 0,'name': 'Y_M_0','precision': 0,'type': 6},{'expression': ' 0.2640*"M_0_sum" - 0.0960*"M_5_sum" + 0.0400*"M_10_sum" - 0.0080*"M_15_sum"  ','length': 0,'name': 'Y_M_1','precision': 0,'type': 6},{'expression': ' 0.1840*"M_0_sum" + 0.0400*"M_5_sum" - 0.0320*"M_10_sum" + 0.0080*"M_15_sum" ','length': 0,'name': 'Y_M_2','precision': 0,'type': 6},{'expression': ' 0.1200*"M_0_sum" + 0.1360*"M_5_sum" - 0.0720*"M_10_sum" + 0.0160*"M_15_sum"  ','length': 0,'name': 'Y_M_3','precision': 0,'type': 6},{'expression': ' 0.0336*"M_0_sum" + 0.2272*"M_5_sum" - 0.0752*"M_10_sum" + 0.0144*"M_15_sum" ','length': 0,'name': 'Y_M_5','precision': 0,'type': 6},{'expression': ' 0.0080*"M_0_sum" + 0.2320*"M_5_sum" - 0.0480*"M_10_sum" + 0.0080*"M_15_sum" ','length': 0,'name': 'Y_M_6','precision': 0,'type': 6},{'expression': ' -0.0080*"M_0_sum" + 0.2160*"M_5_sum" - 0.0080*"M_10_sum" + 0.0000*"M_15_sum"','length': 0,'name': 'Y_M_7','precision': 0,'type': 6},{'expression': ' -0.0160*"M_0_sum" + 0.1840*"M_5_sum" + 0.0400*"M_10_sum" - 0.0080*"M_15_sum"','length': 0,'name': 'Y_M_8','precision': 0,'type': 6},{'expression': ' -0.0128*"M_0_sum" + 0.0848*"M_5_sum" + 0.1504*"M_10_sum" - 0.0240*"M_15_sum" + 0.0016*"M_20_sum"','length': 0,'name': 'Y_M_10','precision': 0,'type': 6},{'expression': ' -0.0016*"M_0_sum" + 0.0144*"M_5_sum" + 0.2224*"M_10_sum" - 0.0416*"M_15_sum" + 0.0064*"M_20_sum"','length': 0,'name': 'Y_M_11','precision': 0,'type': 6},{'expression': ' 0.0064*"M_0_sum" - 0.0336*"M_5_sum" + 0.2544*"M_10_sum" - 0.0336*"M_15_sum" + 0.0064*"M_20_sum"','length': 0,'name': 'Y_M_12','precision': 0,'type': 6},{'expression': ' 0.0064*"M_0_sum" - 0.0416*"M_5_sum" + 0.2224*"M_10_sum" + 0.0144*"M_15_sum" - 0.0016*"M_20_sum"','length': 0,'name': 'Y_M_13','precision': 0,'type': 6},{'expression': ' -0.0128*"M_5_sum" + 0.0848*"M_10_sum" + 0.1504*"M_15_sum" - 0.0240*"M_20_sum" + 0.0016*"M_25_sum"','length': 0,'name': 'Y_M_15','precision': 0,'type': 6},{'expression': ' -0.0016*"M_5_sum" + 0.0144*"M_10_sum" + 0.2224*"M_15_sum" - 0.0416*"M_20_sum" + 0.0064*"M_25_sum"','length': 0,'name': 'Y_M_16','precision': 0,'type': 6},{'expression': ' 0.0064*"M_5_sum" - 0.0336*"M_10_sum" + 0.2544*"M_15_sum" - 0.0336*"M_20_sum" + 0.0064*"M_25_sum"','length': 0,'name': 'Y_M_17','precision': 0,'type': 6},{'expression': ' 0.0064*"M_5_sum" - 0.0416*"M_10_sum" + 0.2224*"M_15_sum" + 0.0144*"M_20_sum" - 0.0016*"M_25_sum"','length': 0,'name': 'Y_M_18','precision': 0,'type': 6},{'expression': ' -0.0128*"M_10_sum" + 0.0848*"M_15_sum" + 0.1504*"M_20_sum" - 0.0240*"M_25_sum" + 0.0016*"M_30_sum"','length': 0,'name': 'Y_M_20','precision': 0,'type': 6},{'expression': ' -0.0016*"M_10_sum" + 0.0144*"M_15_sum" + 0.2224*"M_20_sum" - 0.0416*"M_25_sum" + 0.0064*"M_30_sum"','length': 0,'name': 'Y_M_21','precision': 0,'type': 6},{'expression': ' 0.0064*"M_10_sum" - 0.0336*"M_15_sum" + 0.2544*"M_20_sum" - 0.0336*"M_25_sum" + 0.0064*"M_30_sum"','length': 0,'name': 'Y_M_22','precision': 0,'type': 6},{'expression': ' 0.0064*"M_10_sum" - 0.0416*"M_15_sum" + 0.2224*"M_20_sum" + 0.0144*"M_25_sum" - 0.0016*"M_30_sum"','length': 0,'name': 'Y_M_23','precision': 0,'type': 6},{'expression': ' -0.0128*"M_15_sum" + 0.0848*"M_20_sum" + 0.1504*"M_25_sum" - 0.0240*"M_30_sum" + 0.0016*"M_35_sum"','length': 0,'name': 'Y_M_25','precision': 0,'type': 6},{'expression': ' -0.0016*"M_15_sum" + 0.0144*"M_20_sum" + 0.2224*"M_25_sum" - 0.0416*"M_30_sum" + 0.0064*"M_35_sum"','length': 0,'name': 'Y_M_26','precision': 0,'type': 6},{'expression': ' 0.0064*"M_15_sum" - 0.0336*"M_20_sum" + 0.2544*"M_25_sum" - 0.0336*"M_30_sum" + 0.0064*"M_35_sum"','length': 0,'name': 'Y_M_27','precision': 0,'type': 6},{'expression': ' 0.0064*"M_15_sum" - 0.0416*"M_20_sum" + 0.2224*"M_25_sum" + 0.0144*"M_30_sum" - 0.0016*"M_35_sum"','length': 0,'name': 'Y_M_28','precision': 0,'type': 6},{'expression': ' 0.3616*"F_0_sum" - 0.2768*"F_5_sum" + 0.1488*"F_10_sum" - 0.0336*"F_15_sum" ','length': 0,'name': 'Y_F_0','precision': 0,'type': 6},{'expression': ' 0.2640*"F_0_sum" - 0.0960*"F_5_sum" + 0.0400*"F_10_sum" - 0.0080*"F_15_sum" ','length': 0,'name': 'Y_F_1','precision': 0,'type': 6},{'expression': ' 0.1840*"F_0_sum" + 0.0400*"F_5_sum" - 0.0320*"F_10_sum" + 0.0080*"F_15_sum" ','length': 0,'name': 'Y_F_2','precision': 0,'type': 6},{'expression': ' 0.1200*"F_0_sum" + 0.1360*"F_5_sum" - 0.0720*"F_10_sum" + 0.0160*"F_15_sum" ','length': 0,'name': 'Y_F_3','precision': 0,'type': 6},{'expression': ' 0.0336*"F_0_sum" + 0.2272*"F_5_sum" - 0.0752*"F_10_sum" + 0.0144*"F_15_sum"','length': 0,'name': 'Y_F_5','precision': 0,'type': 6},{'expression': ' 0.0080*"F_0_sum" + 0.2320*"F_5_sum" - 0.0480*"F_10_sum" + 0.0080*"F_15_sum" ','length': 0,'name': 'Y_F_6','precision': 0,'type': 6},{'expression': ' -0.0080*"F_0_sum" + 0.2160*"F_5_sum" - 0.0080*"F_10_sum" + 0.0000*"F_15_sum"','length': 0,'name': 'Y_F_7','precision': 0,'type': 6},{'expression': ' -0.0160*"F_0_sum" + 0.1840*"F_5_sum" + 0.0400*"F_10_sum" - 0.0080*"F_15_sum"','length': 0,'name': 'Y_F_8','precision': 0,'type': 6},{'expression': ' -0.0128*"F_0_sum" + 0.0848*"F_5_sum" + 0.1504*"F_10_sum" - 0.0240*"F_15_sum" + 0.0016*"F_20_sum"','length': 0,'name': 'Y_F_10','precision': 0,'type': 6},{'expression': ' -0.0016*"F_0_sum" + 0.0144*"F_5_sum" + 0.2224*"F_10_sum" - 0.0416*"F_15_sum" + 0.0064*"F_20_sum"','length': 0,'name': 'Y_F_11','precision': 0,'type': 6},{'expression': ' 0.0064*"F_0_sum" - 0.0336*"F_5_sum" + 0.2544*"F_10_sum" - 0.0336*"F_15_sum" + 0.0064*"F_20_sum"','length': 0,'name': 'Y_F_12','precision': 0,'type': 6},{'expression': ' 0.0064*"F_0_sum" - 0.0416*"F_5_sum" + 0.2224*"F_10_sum" + 0.0144*"F_15_sum" - 0.0016*"F_20_sum"','length': 0,'name': 'Y_F_13','precision': 0,'type': 6},{'expression': ' -0.0128*"F_5_sum" + 0.0848*"F_10_sum" + 0.1504*"F_15_sum" - 0.0240*"F_20_sum" + 0.0016*"F_25_sum"','length': 0,'name': 'Y_F_15','precision': 0,'type': 6},{'expression': ' -0.0016*"F_5_sum" + 0.0144*"F_10_sum" + 0.2224*"F_15_sum" - 0.0416*"F_20_sum" + 0.0064*"F_25_sum"','length': 0,'name': 'Y_F_16','precision': 0,'type': 6},{'expression': ' 0.0064*"F_5_sum" - 0.0336*"F_10_sum" + 0.2544*"F_15_sum" - 0.0336*"F_20_sum" + 0.0064*"F_25_sum"','length': 0,'name': 'Y_F_17','precision': 0,'type': 6},{'expression': ' 0.0064*"F_5_sum" - 0.0416*"F_10_sum" + 0.2224*"F_15_sum" + 0.0144*"F_20_sum" - 0.0016*"F_25_sum"','length': 0,'name': 'Y_F_18','precision': 0,'type': 6},{'expression': ' -0.0128*"F_10_sum" + 0.0848*"F_15_sum" + 0.1504*"F_20_sum" - 0.0240*"F_25_sum" + 0.0016*"F_30_sum"','length': 0,'name': 'Y_F_20','precision': 0,'type': 6},{'expression': ' -0.0016*"F_10_sum" + 0.0144*"F_15_sum" + 0.2224*"F_20_sum" - 0.0416*"F_25_sum" + 0.0064*"F_30_sum"','length': 0,'name': 'Y_F_21','precision': 0,'type': 6},{'expression': ' 0.0064*"F_10_sum" - 0.0336*"F_15_sum" + 0.2544*"F_20_sum" - 0.0336*"F_25_sum" + 0.0064*"F_30_sum"','length': 0,'name': 'Y_F_22','precision': 0,'type': 6},{'expression': ' 0.0064*"F_10_sum" - 0.0416*"F_15_sum" + 0.2224*"F_20_sum" + 0.0144*"F_25_sum" - 0.0016*"F_30_sum"','length': 0,'name': 'Y_F_23','precision': 0,'type': 6},{'expression': ' -0.0128*"F_15_sum" + 0.0848*"F_20_sum" + 0.1504*"F_25_sum" - 0.0240*"F_30_sum" + 0.0016*"F_35_sum"','length': 0,'name': 'Y_F_25','precision': 0,'type': 6},{'expression': ' -0.0016*"F_15_sum" + 0.0144*"F_20_sum" + 0.2224*"F_25_sum" - 0.0416*"F_30_sum" + 0.0064*"F_35_sum"','length': 0,'name': 'Y_F_26','precision': 0,'type': 6},{'expression': ' 0.0064*"F_15_sum" - 0.0336*"F_20_sum" + 0.2544*"F_25_sum" - 0.0336*"F_30_sum" + 0.0064*"F_35_sum"','length': 0,'name': 'Y_F_27','precision': 0,'type': 6},{'expression': ' 0.0064*"F_15_sum" - 0.0416*"F_20_sum" + 0.2224*"F_25_sum" + 0.0144*"F_30_sum" - 0.0016*"F_35_sum"','length': 0,'name': 'Y_F_28','precision': 0,'type': 6},{'expression': '"M_0_sum"','length': 0,'name': 'M_0_sum','precision': 0,'type': 6},{'expression': '"M_5_sum"','length': 0,'name': 'M_5_sum','precision': 0,'type': 6},{'expression': '"M_10_sum"','length': 0,'name': 'M_10_sum','precision': 0,'type': 6},{'expression': '"M_15_sum"','length': 0,'name': 'M_15_sum','precision': 0,'type': 6},{'expression': '"M_20_sum"','length': 0,'name': 'M_20_sum','precision': 0,'type': 6},{'expression': '"M_25_sum"','length': 0,'name': 'M_25_sum','precision': 0,'type': 6},{'expression': '"F_0_sum"','length': 0,'name': 'F_0_sum','precision': 0,'type': 6},{'expression': '"F_5_sum"','length': 0,'name': 'F_5_sum','precision': 0,'type': 6},{'expression': '"F_10_sum"','length': 0,'name': 'F_10_sum','precision': 0,'type': 6},{'expression': '"F_15_sum"','length': 0,'name': 'F_15_sum','precision': 0,'type': 6},{'expression': '"F_20_sum"','length': 0,'name': 'F_20_sum','precision': 0,'type': 6},{'expression': '"F_25_sum"','length': 0,'name': 'F_25_sum','precision': 0,'type': 6}],
            'INPUT': groups_0_4_output['OUTPUT'],
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
        single_years_age_section_1_output = processing.run('native:refactorfields', alg_params)

        # Calculating single years of age - Section 2
        alg_params = {
            'FIELDS_MAPPING': [{'expression': '"row_number"','length': 0,'name': 'row_number','precision': 0,'type': 2},{'expression': '"M_0_sum" - "Y_M_0" - "Y_M_1" - "Y_M_2" - "Y_M_3"','length': 0,'name': 'Y_M_4','precision': 0,'type': 6},{'expression': '"M_5_sum" - "Y_M_5" - "Y_M_6" - "Y_M_7" - "Y_M_8"','length': 0,'name': 'Y_M_9','precision': 0,'type': 6},{'expression': '"M_10_sum" - "Y_M_10" - "Y_M_11" - "Y_M_12" - "Y_M_13"','length': 0,'name': 'Y_M_14','precision': 0,'type': 6},{'expression': '"M_15_sum" - "Y_M_15" - "Y_M_16" - "Y_M_17" - "Y_M_18"','length': 0,'name': 'Y_M_19','precision': 0,'type': 6},{'expression': '"M_20_sum" - "Y_M_20" - "Y_M_21" - "Y_M_22" - "Y_M_23"','length': 0,'name': 'Y_M_24','precision': 0,'type': 6},{'expression': '"M_25_sum" - "Y_M_25" - "Y_M_26" - "Y_M_27" - "Y_M_28"','length': 0,'name': 'Y_M_29','precision': 0,'type': 6},{'expression': '"F_0_sum" - "Y_F_0" - "Y_F_1" - "Y_F_2" - "Y_F_3"','length': 0,'name': 'Y_F_4','precision': 0,'type': 6},{'expression': '"F_5_sum" - "Y_F_5" - "Y_F_6" - "Y_F_7" - "Y_F_8"','length': 0,'name': 'Y_F_9','precision': 0,'type': 6},{'expression': '"F_10_sum" - "Y_F_10" - "Y_F_11" - "Y_F_12" - "Y_F_13"','length': 0,'name': 'Y_F_14','precision': 0,'type': 6},{'expression': '"F_15_sum" - "Y_F_15" - "Y_F_16" - "Y_F_17" - "Y_F_18"','length': 0,'name': 'Y_F_19','precision': 0,'type': 6},{'expression': '"F_20_sum" - "Y_F_20" - "Y_F_21" - "Y_F_22" - "Y_F_23"','length': 0,'name': 'Y_F_24','precision': 0,'type': 6},{'expression': '"F_25_sum" - "Y_F_25" - "Y_F_26" - "Y_F_27" - "Y_F_28"','length': 0,'name': 'Y_F_29','precision': 0,'type': 6},{'expression': '"Y_M_0" + "Y_F_0"','length': 0,'name': 'Y_T_0','precision': 0,'type': 6},{'expression': '"Y_M_1" + "Y_F_1"','length': 0,'name': 'Y_T_1','precision': 0,'type': 6},{'expression': ' "Y_M_2" + "Y_F_2"','length': 0,'name': 'Y_T_2','precision': 0,'type': 6},{'expression': '"Y_M_3" + "Y_F_3"','length': 0,'name': 'Y_T_3','precision': 0,'type': 6},{'expression': '"Y_M_5" + "Y_F_5"','length': 0,'name': 'Y_T_5','precision': 0,'type': 6},{'expression': '"Y_M_6" + "Y_F_6"','length': 0,'name': 'Y_T_6','precision': 0,'type': 6},{'expression': '"Y_M_7" + "Y_F_7"','length': 0,'name': 'Y_T_7','precision': 0,'type': 6},{'expression': '"Y_M_8" + "Y_F_8"','length': 0,'name': 'Y_T_8','precision': 0,'type': 6},{'expression': '"Y_M_10" + "Y_F_10"','length': 0,'name': 'Y_T_10','precision': 0,'type': 6},{'expression': '"Y_M_11" + "Y_F_11"','length': 0,'name': 'Y_T_11','precision': 0,'type': 6},{'expression': '"Y_M_12" + "Y_F_12"','length': 0,'name': 'Y_T_12','precision': 0,'type': 6},{'expression': '"Y_M_13" + "Y_F_13"','length': 0,'name': 'Y_T_13','precision': 0,'type': 6},{'expression': ' "Y_M_15" + "Y_F_15"','length': 0,'name': 'Y_T_15','precision': 0,'type': 6},{'expression': '"Y_M_16" + "Y_F_16"','length': 0,'name': 'Y_T_16','precision': 0,'type': 6},{'expression': '"Y_M_17" + "Y_F_17"','length': 0,'name': 'Y_T_17','precision': 0,'type': 6},{'expression': '"Y_M_18" + "Y_F_18"','length': 0,'name': 'Y_T_18','precision': 0,'type': 6},{'expression': '"Y_M_20" + "Y_F_20"','length': 0,'name': 'Y_T_20','precision': 0,'type': 6},{'expression': '"Y_M_21" + "Y_F_21"','length': 0,'name': 'Y_T_21','precision': 0,'type': 6},{'expression': '"Y_M_22" + "Y_F_22"','length': 0,'name': 'Y_T_22','precision': 0,'type': 6},{'expression': ' "Y_M_23" + "Y_F_23"','length': 0,'name': 'Y_T_23','precision': 0,'type': 6},{'expression': '"Y_M_25" + "Y_F_25"','length': 0,'name': 'Y_T_25','precision': 0,'type': 6},{'expression': '"Y_M_26" + "Y_F_26"','length': 0,'name': 'Y_T_26','precision': 0,'type': 6},{'expression': '"Y_M_27" + "Y_F_27"','length': 0,'name': 'Y_T_27','precision': 0,'type': 6},{'expression': '"Y_M_28" + "Y_F_28"','length': 0,'name': 'Y_T_28','precision': 0,'type': 6}],
            'INPUT': single_years_age_section_1_output['OUTPUT'],
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
        single_years_age_section_2_output = processing.run('native:refactorfields', alg_params)

        # Calculating single years of age - Section 3
        alg_params = {
            'FIELDS_MAPPING': [{'expression': '"row_number"','length': 0,'name': 'row_number','precision': 0,'type': 2}, {'expression': '"Y_M_4" + "Y_F_4"','length': 0,'name': 'Y_T_4','precision': 0,'type': 6},{'expression': '"Y_M_9" + "Y_F_9"','length': 0,'name': 'Y_T_9','precision': 0,'type': 6},{'expression': '"Y_M_14" + "Y_F_14"','length': 0,'name': 'Y_T_14','precision': 0,'type': 6},{'expression': '"Y_M_19" + "Y_F_19"','length': 0,'name': 'Y_T_19','precision': 0,'type': 6},{'expression': '"Y_M_24" + "Y_F_24"','length': 0,'name': 'Y_T_24','precision': 0,'type': 6},{'expression': '"Y_M_29" + "Y_F_29"','length': 0,'name': 'Y_T_29','precision': 0,'type': 6}],
            'INPUT': single_years_age_section_2_output['OUTPUT'],
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
        single_years_age_section_3_output = processing.run('native:refactorfields', alg_params)

        # Join attributes by field - Section 1 and 2
        alg_params = {'INPUT':single_years_age_section_1_output['OUTPUT'],
                      'FIELD':'row_number',
                      'INPUT_2':single_years_age_section_2_output['OUTPUT'],
                      'FIELD_2':'row_number',
                      'FIELDS_TO_COPY':[],
                      'METHOD':1,
                      'DISCARD_NONMATCHING':False,
                      'PREFIX':'',
                      'OUTPUT':'TEMPORARY_OUTPUT'}
        join_attributes_location_section_1_2_output = processing.run("native:joinattributestable", alg_params)

        # Join attributes by field - Sections 1 through 3
        alg_params = {'INPUT':join_attributes_location_section_1_2_output['OUTPUT'],
                      'FIELD':'row_number',
                      'INPUT_2':single_years_age_section_3_output['OUTPUT'],
                      'FIELD_2':'row_number',
                      'FIELDS_TO_COPY':[],
                      'METHOD':1,
                      'DISCARD_NONMATCHING':False,
                      'PREFIX':'',
                      'OUTPUT':'TEMPORARY_OUTPUT'}
        join_attributes_location_section_1_2_3_output = processing.run("native:joinattributestable", alg_params)

        # Reorganizing the results
        final_output_sprague = os.path.join(os.getcwd(), country, country + "_" + str(year) + "_Single_years_age.gpkg")
        alg_params = {
            'FIELDS_MAPPING': [{'expression': '"Y_M_0"','length': 0,'name': 'Y_M_0','precision': 0,'type': 6},{'expression': '"Y_M_1"','length': 0,'name': 'Y_M_1','precision': 0,'type': 6},{'expression': '"Y_M_2"','length': 0,'name': 'Y_M_2','precision': 0,'type': 6},{'expression': '"Y_M_3"','length': 0,'name': 'Y_M_3','precision': 0,'type': 6},{'expression': '"Y_M_4"','length': 0,'name': 'Y_M_4','precision': 0,'type': 6},{'expression': '"Y_M_5"','length': 0,'name': 'Y_M_5','precision': 0,'type': 6},{'expression': '"Y_M_6"','length': 0,'name': 'Y_M_6','precision': 0,'type': 6},{'expression': '"Y_M_7"','length': 0,'name': 'Y_M_7','precision': 0,'type': 6},{'expression': '"Y_M_8"','length': 0,'name': 'Y_M_8','precision': 0,'type': 6},{'expression': '"Y_M_9"','length': 0,'name': 'Y_M_9','precision': 0,'type': 6},{'expression': '"Y_M_10"','length': 0,'name': 'Y_M_10','precision': 0,'type': 6},{'expression': '"Y_M_11"','length': 0,'name': 'Y_M_11','precision': 0,'type': 6},{'expression': '"Y_M_12"','length': 0,'name': 'Y_M_12','precision': 0,'type': 6},{'expression': '"Y_M_13"','length': 0,'name': 'Y_M_13','precision': 0,'type': 6},{'expression': '"Y_M_14"','length': 0,'name': 'Y_M_14','precision': 0,'type': 6},{'expression': '"Y_M_15"','length': 0,'name': 'Y_M_15','precision': 0,'type': 6},{'expression': '"Y_M_16"','length': 0,'name': 'Y_M_16','precision': 0,'type': 6},{'expression': '"Y_M_17"','length': 0,'name': 'Y_M_17','precision': 0,'type': 6},{'expression': '"Y_M_18"','length': 0,'name': 'Y_M_18','precision': 0,'type': 6},{'expression': '"Y_M_19"','length': 0,'name': 'Y_M_19','precision': 0,'type': 6},{'expression': '"Y_M_20"','length': 0,'name': 'Y_M_20','precision': 0,'type': 6},{'expression': '"Y_M_21"','length': 0,'name': 'Y_M_21','precision': 0,'type': 6},{'expression': '"Y_M_22"','length': 0,'name': 'Y_M_22','precision': 0,'type': 6},{'expression': '"Y_M_23"','length': 0,'name': 'Y_M_23','precision': 0,'type': 6},{'expression': '"Y_M_24"','length': 0,'name': 'Y_M_24','precision': 0,'type': 6},{'expression': '"Y_M_25"','length': 0,'name': 'Y_M_25','precision': 0,'type': 6},{'expression': '"Y_M_26"','length': 0,'name': 'Y_M_26','precision': 0,'type': 6},{'expression': '"Y_M_27"','length': 0,'name': 'Y_M_27','precision': 0,'type': 6},{'expression': '"Y_M_28"','length': 0,'name': 'Y_M_28','precision': 0,'type': 6},{'expression': '"Y_M_29"','length': 0,'name': 'Y_M_29','precision': 0,'type': 6},{'expression': '"Y_F_0"','length': 0,'name': 'Y_F_0','precision': 0,'type': 6},{'expression': '"Y_F_1"','length': 0,'name': 'Y_F_1','precision': 0,'type': 6},{'expression': '"Y_F_2"','length': 0,'name': 'Y_F_2','precision': 0,'type': 6},{'expression': '"Y_F_3"','length': 0,'name': 'Y_F_3','precision': 0,'type': 6},{'expression': '"Y_F_4"','length': 0,'name': 'Y_F_4','precision': 0,'type': 6},{'expression': '"Y_F_5"','length': 0,'name': 'Y_F_5','precision': 0,'type': 6},{'expression': '"Y_F_6"','length': 0,'name': 'Y_F_6','precision': 0,'type': 6},{'expression': '"Y_F_7"','length': 0,'name': 'Y_F_7','precision': 0,'type': 6},{'expression': '"Y_F_8"','length': 0,'name': 'Y_F_8','precision': 0,'type': 6},{'expression': '"Y_F_9"','length': 0,'name': 'Y_F_9','precision': 0,'type': 6},{'expression': '"Y_F_10"','length': 0,'name': 'Y_F_10','precision': 0,'type': 6},{'expression': '"Y_F_11"','length': 0,'name': 'Y_F_11','precision': 0,'type': 6},{'expression': '"Y_F_12"','length': 0,'name': 'Y_F_12','precision': 0,'type': 6},{'expression': '"Y_F_13"','length': 0,'name': 'Y_F_13','precision': 0,'type': 6},{'expression': '"Y_F_14"','length': 0,'name': 'Y_F_14','precision': 0,'type': 6},{'expression': '"Y_F_15"','length': 0,'name': 'Y_F_15','precision': 0,'type': 6},{'expression': '"Y_F_16"','length': 0,'name': 'Y_F_16','precision': 0,'type': 6},{'expression': '"Y_F_17"','length': 0,'name': 'Y_F_17','precision': 0,'type': 6},{'expression': '"Y_F_18"','length': 0,'name': 'Y_F_18','precision': 0,'type': 6},{'expression': '"Y_F_19"','length': 0,'name': 'Y_F_19','precision': 0,'type': 6},{'expression': '"Y_F_20"','length': 0,'name': 'Y_F_20','precision': 0,'type': 6},{'expression': '"Y_F_21"','length': 0,'name': 'Y_F_21','precision': 0,'type': 6},{'expression': '"Y_F_22"','length': 0,'name': 'Y_F_22','precision': 0,'type': 6},{'expression': '"Y_F_23"','length': 0,'name': 'Y_F_23','precision': 0,'type': 6},{'expression': '"Y_F_24"','length': 0,'name': 'Y_F_24','precision': 0,'type': 6},{'expression': '"Y_F_25"','length': 0,'name': 'Y_F_25','precision': 0,'type': 6},{'expression': '"Y_F_26"','length': 0,'name': 'Y_F_26','precision': 0,'type': 6},{'expression': '"Y_F_27"','length': 0,'name': 'Y_F_27','precision': 0,'type': 6},{'expression': '"Y_F_28"','length': 0,'name': 'Y_F_28','precision': 0,'type': 6},{'expression': '"Y_F_29"','length': 0,'name': 'Y_F_29','precision': 0,'type': 6},{'expression': '"Y_T_0"','length': 0,'name': 'Y_T_0','precision': 0,'type': 6},{'expression': '"Y_T_1"','length': 0,'name': 'Y_T_1','precision': 0,'type': 6},{'expression': '"Y_T_2"','length': 0,'name': 'Y_T_2','precision': 0,'type': 6},{'expression': '"Y_T_3"','length': 0,'name': 'Y_T_3','precision': 0,'type': 6},{'expression': '"Y_T_4"','length': 0,'name': 'Y_T_4','precision': 0,'type': 6},{'expression': '"Y_T_5"','length': 0,'name': 'Y_T_5','precision': 0,'type': 6},{'expression': '"Y_T_6"','length': 0,'name': 'Y_T_6','precision': 0,'type': 6},{'expression': '"Y_T_7"','length': 0,'name': 'Y_T_7','precision': 0,'type': 6},{'expression': '"Y_T_8"','length': 0,'name': 'Y_T_8','precision': 0,'type': 6},{'expression': '"Y_T_9"','length': 0,'name': 'Y_T_9','precision': 0,'type': 6},{'expression': '"Y_T_10"','length': 0,'name': 'Y_T_10','precision': 0,'type': 6},{'expression': '"Y_T_11"','length': 0,'name': 'Y_T_11','precision': 0,'type': 6},{'expression': '"Y_T_12"','length': 0,'name': 'Y_T_12','precision': 0,'type': 6},{'expression': '"Y_T_13"','length': 0,'name': 'Y_T_13','precision': 0,'type': 6},{'expression': '"Y_T_14"','length': 0,'name': 'Y_T_14','precision': 0,'type': 6},{'expression': '"Y_T_15"','length': 0,'name': 'Y_T_15','precision': 0,'type': 6},{'expression': '"Y_T_16"','length': 0,'name': 'Y_T_16','precision': 0,'type': 6},{'expression': '"Y_T_17"','length': 0,'name': 'Y_T_17','precision': 0,'type': 6},{'expression': '"Y_T_18"','length': 0,'name': 'Y_T_18','precision': 0,'type': 6},{'expression': '"Y_T_19"','length': 0,'name': 'Y_T_19','precision': 0,'type': 6},{'expression': '"Y_T_20"','length': 0,'name': 'Y_T_20','precision': 0,'type': 6},{'expression': '"Y_T_21"','length': 0,'name': 'Y_T_21','precision': 0,'type': 6},{'expression': '"Y_T_22"','length': 0,'name': 'Y_T_22','precision': 0,'type': 6},{'expression': '"Y_T_23"','length': 0,'name': 'Y_T_23','precision': 0,'type': 6},{'expression': '"Y_T_24"','length': 0,'name': 'Y_T_24','precision': 0,'type': 6},{'expression': '"Y_T_25"','length': 0,'name': 'Y_T_25','precision': 0,'type': 6},{'expression': '"Y_T_26"','length': 0,'name': 'Y_T_26','precision': 0,'type': 6},{'expression': '"Y_T_27"','length': 0,'name': 'Y_T_27','precision': 0,'type': 6},{'expression': '"Y_T_28"','length': 0,'name': 'Y_T_28','precision': 0,'type': 6},{'expression': '"Y_T_29"','length': 0,'name': 'Y_T_29','precision': 0,'type': 6}],
            'INPUT': join_attributes_location_section_1_2_3_output['OUTPUT'],
            'OUTPUT': final_output_sprague
        }
        processing.run('native:refactorfields', alg_params)
        print("Finished with year " + str(year))
    os.remove(bounding_box)
    print("Finished processing " + country)
end = time.time()
print("Number of seconds it took to finish the calculations: " + str(end - start))
